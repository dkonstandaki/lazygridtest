package com.dk.lazygridtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ArrayList<Integer> imgs = new ArrayList<>();
        imgs.add(R.drawable.img_1);
        imgs.add(R.drawable.img_2);
        imgs.add(R.drawable.img_3);
        imgs.add(R.drawable.img_4);
        imgs.add(R.drawable.img_5);
        imgs.add(R.drawable.img_6);
        imgs.add(R.drawable.img_7);
        imgs.add(R.drawable.img_8);
        imgs.add(R.drawable.img_9);
        imgs.add(R.drawable.img_10);
        imgs.add(R.drawable.img_11);
        imgs.add(R.drawable.img_12);

        GridView gridView = (GridView) findViewById(R.id.gridView);
        GridAdapter gridAdapter = new GridAdapter(this, imgs);
        gridView.setAdapter(gridAdapter);
    }
}
