package com.dk.lazygridtest;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.support.v4.util.LruCache;
import android.widget.ImageView;

import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageLoader {

    private Context mContext;
    private Map<ImageView, Integer> imageViews = Collections.synchronizedMap(new WeakHashMap<ImageView, Integer>());
    private ExecutorService executorService;
    private Handler handler = new Handler();
    private LruCache<String, Bitmap> mMemoryCache;

    public ImageLoader(Context context) {
        mContext = context;
        executorService = Executors.newFixedThreadPool(5);
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        final int cacheSize = maxMemory / 8;
        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    public void displayImage(Integer resId, ImageView imageView) {
        imageViews.put(imageView, resId);
        Bitmap bitmap = getBitmapFromMemCache(resId.toString());
        if (bitmap != null)
            imageView.setImageBitmap(bitmap);
        else
            queuePhoto(resId, imageView);

    }

    private void queuePhoto(int resId, ImageView imageView) {
        PhotoToLoad p = new PhotoToLoad(resId, imageView);
        executorService.submit(new PhotosLoader(p));
    }

    private static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            final int halfHeight = height / 2;
            final int halfWidth = width / 2;
            while ((halfHeight / inSampleSize) >= reqHeight && (halfWidth / inSampleSize) >= reqWidth)
                inSampleSize *= 2;
        }
        return inSampleSize;
    }

    private class PhotoToLoad {
        int resId;
        ImageView imageView;

        PhotoToLoad(int resId, ImageView imageView) {
            this.resId = resId;
            this.imageView = imageView;
        }
    }

    private class PhotosLoader implements Runnable {

        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad) {
            this.photoToLoad = photoToLoad;
        }

        @Override
        public void run() {
            try {
                if (imageViewReused(photoToLoad))
                    return;
                Bitmap bmp = decodeSampledBitmapFromResource(mContext.getResources(), photoToLoad.resId, 100, 100);
                addBitmapToMemoryCache(String.valueOf(photoToLoad.resId), bmp);
                if (imageViewReused(photoToLoad))
                    return;
                BitmapDisplayer bd = new BitmapDisplayer(bmp, photoToLoad);
                handler.post(bd);
            } catch(Throwable th) {
                th.printStackTrace();
            }
        }
    }

    private boolean imageViewReused(PhotoToLoad photoToLoad) {
        Integer resId = imageViews.get(photoToLoad.imageView);
        return resId == null || !resId.equals(photoToLoad.resId);
    }

    private class BitmapDisplayer implements Runnable {

        Bitmap bitmap;
        PhotoToLoad photoToLoad;

        BitmapDisplayer(Bitmap b, PhotoToLoad p){
            bitmap = b;
            photoToLoad = p;
        }

        public void run() {
            if (imageViewReused(photoToLoad))
                return;
            if (bitmap != null)
                photoToLoad.imageView.setImageBitmap(bitmap);
        }
    }

    private void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    private Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }
}
