package com.dk.lazygridtest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.List;

public class GridAdapter extends BaseAdapter {

    private List<Integer> mItems = null;
    private LayoutInflater mInflater;
    private ImageLoader mImageLoader;

    public GridAdapter(Context context, List<Integer> items) {
        mItems = items;
        mInflater = LayoutInflater.from(context);
        mImageLoader = new ImageLoader(context.getApplicationContext());
    }

    public int getCount() {
        return mItems.size();
    }

    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int i) {
        return mItems.get(i);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.grid_item, null);
            holder = new ViewHolder();
            holder.ivImage = (ImageView) convertView.findViewById(R.id.ivGridImg);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        mImageLoader.displayImage(mItems.get(position), holder.ivImage);
        return convertView;
    }

    private static class ViewHolder {
        ImageView ivImage;
    }
}
